<?php
use ArtfulRobot\Shelf\AssetController;
use ArtfulRobot\Shelf\PageController;
use ArtfulRobot\Shelf\IndexController;
use ArtfulRobot\Shelf\Core;

//if (preg_match('/\.(?:png|jpg|jpeg|gif|svg)$/', $_SERVER["REQUEST_URI"])) {
//    return false;    // serve the requested resource as-is.
//}

include_once __DIR__ . '/../vendor/autoload.php';

define('SHELF_DATA_DIR', __DIR__ . '/data');
define('SHELF_CONFIG_FILE', __DIR__ . '/../shelf.json');
define('SHELF_FAVICON', __DIR__ . '/favicon.ico');
$core = Core::singleton();

$request = parse_url(rawurldecode($_SERVER['REQUEST_URI']));
$request['query'] = $_GET ?? [];

if (preg_match('/\.(html|md)$/', $request['path'])) {
  // Requesting a particular rendered page.
  PageController::handle($core, $request);
}
elseif (preg_match('/\.(?:png|jpg|jpeg|gif|svg)$/', $request['path'])) {
  AssetController::handle($core, $request);
}
elseif ('/favicon.ico' === $request['path']) {
  header('Content-Type: ' . mime_content_type(SHELF_FAVICON));
  readfile(SHELF_FAVICON);
  exit;
}
else {
  IndexController::handle($core, $request);
}
