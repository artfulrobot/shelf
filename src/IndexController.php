<?php
namespace ArtfulRobot\Shelf;


class IndexController extends Controller {

  public function run() {

    $this->scan($_GET['force'] ?? 0);

    $search = trim(mb_strtolower($_GET['search'] ?? ''));
    if ($search) {
      $search = preg_split('/\s+/', $search);
      $searchValue = htmlspecialchars(implode(" ", $search));
    }
    else {
      $search = NULL;
      $searchValue = '';
    }

    $html = '';
    foreach ($this->core->index as $indexedProject) {
      $html .= "<article><h2>" . htmlspecialchars($indexedProject['name']) . '</h2>';
      $project = new Project($this->core, $this->core->projectSlugToConfig[$indexedProject['slug']]);
      $html .= $project->renderPageList($search);
      $html .="</article>\n";
    }
    $clearSearch = empty($searchValue) ? '' : ('Files containing <em>' . $searchValue . '</em>' . '<a href=/ class="clear-search" >Show all</a>');

    $html = <<<HTML
      <div class="index-content">
        <div class="shelf-index-header">
          <h1>Shelf</h1>
          <form method=get><div><span id=hint ><kbd>Enter</kbd> for full text search</span><input name=search data-page-search="$searchValue" /></div> $clearSearch</form>
        </div>
        <div class="projects">$html</div>
      </div>';
      HTML;

    $this->renderPage($html, [
        '{title}' => htmlspecialchars('Shelf'),
        '{pageClass}' => 'index',
      ]);
  }

  /**
   * Update the index with all the .md files from each project.
   */
  public function scan($force) {

    // Build index of files
    $valid = [];
    foreach ($this->core->projectSlugToConfig as $slug => $sourceDir) {
      $project = new Project($this->core, $sourceDir);
      $valid[$slug] = 1;
      $project->indexFiles($force);
    }

    // Remove anything from the index that is not in sourceDirs.
    $this->core->index = array_intersect_key($this->core->index, $valid);

    $this->core->saveIndex();
  }


}
