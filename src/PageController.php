<?php
namespace ArtfulRobot\Shelf;

class PageController extends Controller {

  public function run() {
    if (!empty($this->request['query']['since'])) {
      return $this->runJson();
    }
    return $this->runHtml();
  }

  protected function runHtml() {
    try {
      $projectFile = ProjectFile::fromRelPath($this->core, $this->request['path']);
      $html = $projectFile->getRenderedHeader()
        . $projectFile->getRenderedContent(!empty($this->request['query']['rebuild']));
      $this->renderPage($html, [
        '{title}'       => htmlspecialchars($projectFile->title),
        '{projectNav}'  => '<nav class="project">' . $projectFile->renderPageList() . '</nav>',
        '{pageClass}'   => 'file',
        '{mtimeSource}' => $projectFile->getMtimeSource(),
      ]);
    }
    catch (\InvalidArgumentException $e) {
      $this->renderPage($e->getMessage());
    }

    exit;
  }
  public function runJson() {
    $since = (int) $this->request['query']['since'];
    $projectFile = ProjectFile::fromRelPath($this->core, $this->request['path']);
    if ($projectFile->getMtimeSource() <= $since) {
      // The file has not changed since the browser loaded it
      $this->jsonResponse(['unchanged' => 1]);
    }
    // The file has changed.
    $html = $projectFile->getRenderedContent(!empty($this->request['query']['rebuild']));
    $this->jsonResponse([
      'content'     => $html,
      'title'       => htmlspecialchars($projectFile->title),
      'projectNav'  => '<nav class="project">' . $projectFile->renderPageList() . '</nav>',
      'mtimeSource' => $projectFile->getMtimeSource(),
      //'pageClass'   => 'file',
    ]);
  }

}
