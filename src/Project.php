<?php
namespace ArtfulRobot\Shelf;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class Project {
  public $slug;
  public $name;
  public $dir;
  public $core;

  public function __construct(Core $core, array $sourceDir) {
    $this->slug = $sourceDir['slug'] ?? trim(preg_replace('/[^a-z0-9]+/', '-', strtolower($sourceDir['name'])), '-');
    $this->name = $sourceDir['name'];
    $this->dir = $sourceDir['dir'];
    $this->core = $core;
    // error_log("___ $this->slug $this->dir");
  }

  /**
   * From path like /slug/path/to/file (note lack of .extension)
   */
  public static function fromRelPath(Core $core, string $relPath) :Project {
    $projectSlug = explode('/', $relPath)[1];
    $project = new static($core, $core->projectSlugToConfig[$projectSlug]);
    return $project;
  }

  /**
   * Recursively go through all the .md files in a project
   * creating HTML versions as needed, and updating the index.
   *
   */
  public function indexFiles(bool $force = FALSE) {

    if (!isset($this->core->index[$this->slug])) {
      $this->core->index[$this->slug] = [
        'slug' => $this->slug,
        'name' => $this->name,
        'files' => [],
      ];
    }
    elseif (!$force && (time() - ($this->core->index[$this->slug]['indexedOn'] ?? 0)) < 60) {
      // do not re-index if done 1 minute ago
      error_log("Skipping indexing of " . $this->slug);
      return;
    }
    $files = &$this->core->index[$this->slug]['files'];
    $filesFound = [];
    $di = new RecursiveDirectoryIterator($this->dir, RecursiveDirectoryIterator::SKIP_DOTS);
    $it = new RecursiveIteratorIterator($di);

    // error_log("indexing of " . $this->slug);
    foreach($it as $file) {
      if (pathinfo($file, PATHINFO_EXTENSION) == "md") {

        $projectFile = new ProjectFile($this, $file);
        $filesFound[$projectFile->relPath] = 1;

        $projectFile->updateHtml($force);

        $files[$projectFile->relPath] = [
          'path' => $projectFile->relPath,
          'title' => $projectFile->title,
          'htmlUrl' => $projectFile->getHtmlUrl(),
          'mtime' => $projectFile->getMtimeSource(),
        ];
      }
    }
    // Remove old files no longer present.
    $old = array_diff_key($files, $filesFound);
    // error_log(count($files) . " files found ");
    // error_log(count($old) . " old files being removed:" . json_encode($old));
    foreach ($old as $oldFile) {
      error_log("removing '$oldFile[path]'");
      $projectFile = new ProjectFile($this, $oldFile['path'], TRUE);
      $htmlFile = $projectFile->getHtmlPath();
      if (file_exists($htmlFile)) {
        unlink($htmlFile);
      }
    }
    $this->core->index[$this->slug]['files'] = array_intersect_key($files, $filesFound);

    // Sort.
    uasort($this->core->index[$this->slug]['files'], function ($a, $b) {

      $aIsIndex = substr($a['path'], -6) === '/index';
      $bIsIndex = substr($b['path'], -6) === '/index';

      // Sort by dir.
      $aDirs = array_slice(explode('/', $a['path']), 1);
      $bDirs = array_slice(explode('/', $b['path']), 1);

      // if b has more parts than a, it goes below. And vice-versa.
      if (count($bDirs) > count($aDirs)) return -1;
      if (count($aDirs) > count($bDirs)) return 1;

      // Both a and b have the same number of parts,
      // e.g. /subdir/file1, /subdir/file2,
      //      /subdir/file1, /otherdir/file1
      //      file3,         file4
      $fileA = mb_strtolower(array_pop($aDirs));
      $fileB = mb_strtolower(array_pop($bDirs));
      // Sort dirs
      $cf = mb_strtolower(implode('/', $aDirs)) <=> mb_strtolower(implode('/', $bDirs));
      if ($cf === 0) {
        // both files are in the same dir.
        // Privilege index to the top.
        if ($aIsIndex && !$bIsIndex) return -1;
        if ($bIsIndex && !$aIsIndex) return 1;
        $cf = $fileA <=> $fileB;
      }
      return $cf;
    });

    $this->core->index[$this->slug]['indexedOn'] = time();
  }

  /**
   * @return string HTML
   */
  public function renderPageList(?array $search, ?string $selectedRelPath = NULL) :string {
    $html = '<div class="pages"><ul>';
    $lastSubDir = '<none>';
    $subDirSelected = $selectedRelPath
      ? ltrim(preg_replace(';^(.*/).*$;', '$1', $selectedRelPath), '/')
      : NULL;
    // $html = "SEL:$subDirSelected REL: $selectedRelPath $html";

    foreach ($this->core->index[$this->slug]['files'] ?? [] as $indexedFile) {

      if ($search) {
        // Only return the result if the file search matches.
        $file = new ProjectFile($this, $indexedFile['path'], TRUE);
        if (!$file->matches($search)) {
          continue;
        }
      }

      // Handle entering/leaving subdirs.
      preg_match(';^/(.*/)?(.*)$;', $indexedFile['path'], $matches);
      $subDirPath = $matches[1] ?? '';
      // bug: it thinks cousins are ancestors.
      $currentDirIsAncestor = (substr($subDirSelected, 0, strlen($subDirPath)) === $subDirPath);
      $currentDirIsChild = (substr($subDirPath, 0, strlen($subDirSelected)) === $subDirSelected);
      // $html .= ($currentDirIsChild ? 'CHILD' : 'NOT CHILD') . ': ' . $subDirPath . ' cf ' . $subDirSelected . " cf " . substr($subDirPath, 0, strlen($subDirSelected)) . "<br/>";
      $subDir = $subDirPath
      ? '<div class="subdir' . ($currentDirIsAncestor ? 'trail ' : '') .($currentDirIsChild ? 'child ' : '') . '">'
        . htmlspecialchars($subDirPath) . '</div>'
      : '';
      if ($subDir && $subDir !== $lastSubDir) {
        $html .= '</ul>' . $subDir . '<ul>';
        $lastSubDir = $subDir;
      }

      $daysAgo = (time() - $indexedFile['mtime'])/60/60/24;
      if ($daysAgo < 1) $isRecent = '✨ ';
      elseif ($daysAgo < 7) $isRecent = '⭐️ ';
      else $isRecent = '';// $daysAgo . " " . date('Y-m-d', $indexedFile['mtime']);

      if ($indexedFile['path'] === $selectedRelPath) {
        $html .= "<li class='selected'><span>$isRecent"
          . htmlspecialchars($indexedFile['title'])
          . "</span></li>\n";
      }
      else {
        // Don't render cousins.
        $render = TRUE;
        if ($selectedRelPath) {
          $render = $currentDirIsChild;
        }
        if ($render) {
          $searchable = htmlspecialchars(mb_strtolower("$indexedFile[title] $indexedFile[path] $this->name $this->dir $this->slug"));
          $html .= "<li data-search=\"$searchable\" >". /*(json_encode(['isChild' => $currentDirIsChild, 'isAnc' => $currentDirIsAncestor]))*/  "<a href='{$indexedFile['htmlUrl']}'>$isRecent"
            . htmlspecialchars($indexedFile['title'])
            . "</a></li>\n";
        }
      }
    }
    $html .="</ul></div></article>\n";
    return $html;
  }

}
