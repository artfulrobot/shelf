<?php
namespace ArtfulRobot\Shelf;

class ProjectFile {
  /** Full path */
  public string $mdFilePath;
  /** path relative to the project and without extension */
  public string $relPath;
  public Project $project;
  public string $htmlFilePath;
  public string $title;
  public int $mtimeSource;

  /**
    */
  public static function fromRelPath(Core $core, string $relativePath) {
    if (preg_match('@^/([^/]+)(/.*?)(\.(?:md|html))$@', $relativePath, $matches)) {
      if (!isset($core->projectSlugToConfig[$matches[1]])) {
        throw new \InvalidArgumentException("Project slug $matches[1] not found in projects: " . implode(', ', array_keys($core->projectSlugToConfig)));
      }

      $project = new Project($core, $core->projectSlugToConfig[$matches[1]]);
      $file = new ProjectFile($project, $matches[2], TRUE);
      return $file;
    }
    throw new \InvalidArgumentException("Cannot parse $relativePath");
  }
  /**
   * If $relativePath then $mdFilePath is like/this otherwise it is /home/user/project/like/this.md
   *
   */
  public function __construct(Project $project, string $mdFilePath, bool $relativePath = FALSE) {
    $this->project = $project;
    if ($relativePath) {
      $this->mdFilePath = $project->dir . $mdFilePath . '.md';
      $this->relPath = $mdFilePath;
    }
    else {
      if (substr($mdFilePath, 0, strlen($project->dir)) !== $project->dir) {
        // uh oh.
        throw new \Exception("md path $mdFilePath does not match " . $project->dir);
      }
      $this->mdFilePath = $mdFilePath;
      $mdPathWithoutSuffix = substr($this->mdFilePath, 0, strlen($this->mdFilePath) - 3);
      $this->relPath = substr($mdPathWithoutSuffix, strlen($this->project->dir));
    }

    // Can we load the title from the index?
    $this->title  = $this->project->core->index[$this->project->slug]['files'][$this->relPath]['title'] ?? substr($this->relPath, 1);
  }

  public function getHtmlPath() :string {
    $htmlPath = SHELF_DATA_DIR . '/' . $this->project->slug .
      $this->relPath
      . '.html';
    return $htmlPath;
  }

  public function getHtmlUrl() :string {
    $htmlPath = '/' . $this->project->slug . $this->relPath . '.html';
    return $htmlPath;
  }

  /**
   * Get unixtime stamp of source .md file's mtime.
   */
  public function getMtimeSource(bool $force = FALSE) :int {
    if ($force || !isset($this->mtimeSource)) {
      $this->mtimeSource = filemtime($this->mdFilePath);
    }
    return $this->mtimeSource;
  }
  public function updateHtml(bool $force = FALSE) {

    $htmlPath = $this->getHtmlPath();

    $rebuild = TRUE;

    if (!$force && file_exists($htmlPath)) {
      // compare the mtimes of the two files.
      if (filemtime($htmlPath) >= $this->getMtimeSource()) {
        // HTML is fine.
        $rebuild = FALSE;
      }
    }

    if ($rebuild) {
      $this->createHtml($this);
    }

  }

  public function createHtml() {
    $parsedown = new \ParsedownExtra();
    $parsedown->setMarkupEscaped(true); # escapes markup (HTML)
    $html = '<div class="file-content">' . $parsedown->text(file_get_contents($this->mdFilePath)) . '</div>';

    $htmlPath = $this->getHtmlPath();
    $dir = dirname($htmlPath);
    if (!file_exists($dir)) {
      mkdir($dir, 0777, TRUE);
    }
    file_put_contents($htmlPath, $html);
    // Crudely find the first h1.
    if (preg_match('@<h1>([^<]+)</h1>@s', $html, $matches)) {
      $this->title = $matches[1];
      // error_log('ProjectFile.createHtml: ' . json_encode([$this->relPath, $this->title]));
    }
    else {
      // error_log('ProjectFile.createHtml: ' . json_encode([$this->relPath, 'NO h1']));
      $this->title = substr($this->relPath, 1);
    }
  }
  public function getRenderedHeader() :string {
    $html = '<header><div class="path"><a href="/" class="home" >Shelf</a> <span class="project-dir">' . htmlspecialchars($this->project->dir) . '</span><span class="project-file">' 
      .htmlspecialchars($this->relPath) . '.md</span></div></header>';
    return $html;
  }
  public function matches(array $needles) {
    $src = mb_strtolower(file_get_contents($this->mdFilePath));
    foreach ($needles as $needle) {
      if (strpos($src, $needle) === FALSE) {
        // error_log($this->mdFilePath . " does not contain '$needle'");
        return FALSE;
      }
    }
    error_log($this->mdFilePath . " does contain '$needle'");
    return TRUE;
  }
  /**
   * Render the page list, higlighting this file.
   */
  public function renderPageList() {
    return '<h2>' . htmlspecialchars($this->project->name) . '</h2>'
      . $this->project->renderPageList(NULL, $this->relPath);
  }
  /**
   * Return the rendered markdown.
   *
   * Re-creates the html file if needed/forced.
   */
  public function getRenderedContent(bool $force = FALSE) :string {
    $this->updateHtml($force);
    $path = $this->getHtmlPath();
    if (!file_exists($path)) {
      throw new \InvalidArgumentException("Error: $path does not exist");
    }
    return file_get_contents($path);
  }
}
