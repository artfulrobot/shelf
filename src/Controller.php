<?php
namespace ArtfulRobot\Shelf;

abstract class Controller {

  protected Core $core;

  protected array $request;

  public static function handle(Core $core, array $request) {
    $c = new static($core, $request);
    $c->run();
  }

  public function __construct(Core $core, array $request) {
    $this->core = $core;
    $this->request = $request;
  }

  abstract public function run();

  public function renderPage(string $content, array $substitutions = []) {
    // $head = file_get_contents(SHELF_DATA_DIR . '/../html.html');
    // $content = file_get_contents(SHELF_DATA_DIR . '/../content.html');
    $page = file_get_contents(SHELF_DATA_DIR . '/../page-template.html');

    $mtime = filemtime(SHELF_DATA_DIR . '/../page-template.html');
    $s = time() - $mtime;

    print strtr($page, $substitutions + [
      '{content}' => $content,
      '{tplVersion}' => json_encode("{$s}s ago " . date('Y-m-d H:i:s', $mtime)),
    ]);
  }

  public function renderContent(string $content, array $substitutions = []) {

    $content = file_get_contents(SHELF_DATA_DIR . '/../content.html');
  }

  /**
   * Emit a json response and exit.
   */
  public function jsonResponse($return) {
    $body = json_encode($return);
    header('Content-Type: application/json');
    header('Content-Length: ' . strlen($body));
    print $body;
    exit;
  }
}
